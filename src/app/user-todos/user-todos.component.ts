import { Component, OnInit } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/database';

@Component({
  selector: 'userTodos',
  templateUrl: './user-todos.component.html',
  styleUrls: ['./user-todos.component.css']
})
export class UserTodosComponent implements OnInit {

  user = "Jack";
  todoTextFromTodo = "No text to show";

  showText($event)
  {
    this.todoTextFromTodo = $event;
  }

  todos = [];

  constructor(private db:AngularFireDatabase) { }

  changeUser()
  {
    this.db.list('/users/'+this.user+'/todos').snapshotChanges().subscribe(
      todos => {
        this.todos = [];
        todos.forEach(
          todo => {
            let y = todo.payload.toJSON();
            y["$key"] = todo.key;
            this.todos.push(y);
          }
        )
      }
    )

  }

  ngOnInit() {
    this.db.list('/users/'+this.user+'/todos').snapshotChanges().subscribe(
      todos => {
        this.todos = [];
        todos.forEach(
          todo => {
            let y = todo.payload.toJSON();
            y["$key"] = todo.key;
            this.todos.push(y);
          }
        )
      }
    )

  }

}
