import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';


//firabase modules
import { AngularFireModule } from '@angular/fire';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { AngularFireAuthModule } from '@angular/fire/auth';


/* material design imports */
import {MatCardModule} from '@angular/material/card';
import {MatInputModule} from '@angular/material/input';
import {MatButtonModule} from '@angular/material/button';


import { AppComponent } from './app.component';
import { MainComponent } from './main/main.component';
import { NavComponent } from './nav/nav.component';
import { TodosComponent } from './todos/todos.component';
import { TodoComponent } from './todo/todo.component';
import { RegistrationComponent } from './registration/registration.component';
import { LoginComponent } from './login/login.component';
import { CodesComponent } from './codes/codes.component';
import { Routes, RouterModule } from '@angular/router';


import { environment } from '../environments/environment';
import { UserTodosComponent } from './user-todos/user-todos.component';

@NgModule({
  declarations: [
    AppComponent,
    MainComponent,
    NavComponent,
    TodosComponent,
    TodoComponent,
    RegistrationComponent,
    LoginComponent,
    CodesComponent,
    UserTodosComponent
  ],
  imports: [
    BrowserModule,
    MatCardModule,
    MatInputModule,
    FormsModule,
    MatButtonModule,
    BrowserAnimationsModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    RouterModule.forRoot([
      { path:'', component: TodosComponent },
      { path:'register', component: RegistrationComponent },
      { path:'login', component: LoginComponent },
      { path:'codes', component: CodesComponent },
      { path:'userTodos', component: UserTodosComponent },
      { path:'**', component: TodosComponent }
    ])
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
