import { Injectable } from '@angular/core';
import { AuthService } from './auth.service';
import { AngularFireDatabase, AngularFireList } from '@angular/fire/database';

@Injectable({
  providedIn: 'root'
})
export class TodosService {


  addTodo(text:string)
  {
    this.authService.user.subscribe(user => {
      this.db.list('/users/'+user.uid+'/todos').push({'text':text});
    })
  }

  deleteTodo(key:string)
  {
    this.authService.user.subscribe(user => {
      this.db.list('/users/'+user.uid+'/todos').remove(key);
    })
  }

  updateTodo(key:string, text:string)
  {
    this.authService.user.subscribe(user => {
      this.db.list('/users/'+user.uid+'/todos').update(key, {'text':text});
    })
  }

  constructor(private authService: AuthService,
              private db: AngularFireDatabase) 
              {

               }
}
